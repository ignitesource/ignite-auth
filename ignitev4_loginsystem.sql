-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 24, 2020 at 02:40 PM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ignitev4_loginsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `suff_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `group_id`, `suff_name`, `name`, `created`, `updated`) VALUES
(1, 0, 'user management', 'usermanagement-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(2, 0, 'user management', 'usermanagement-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(3, 0, 'user management', 'usermanagement-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(4, 0, 'user management', 'usermanagement-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(5, 0, 'user management', 'usermanagement-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(6, 4, 'restaurant', 'restaurant-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(7, 4, 'restaurant', 'restaurant-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(8, 4, 'restaurant', 'restaurant-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(9, 4, 'restaurant', 'restaurant-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(10, 4, 'restaurant', 'restaurant-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(11, 4, 'bar', 'bar-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(12, 4, 'bar', 'bar-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(13, 4, 'bar', 'bar-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(14, 4, 'bar', 'bar-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(15, 4, 'bar', 'bar-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(16, 4, 'ktv', 'ktv-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(17, 4, 'ktv', 'ktv-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(18, 4, 'ktv', 'ktv-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(19, 4, 'ktv', 'ktv-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(20, 4, 'ktv', 'ktv-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(21, 4, 'laundry', 'laundry-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(22, 4, 'laundry', 'laundry-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(23, 4, 'laundry', 'laundry-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(24, 4, 'laundry', 'laundry-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(25, 4, 'laundry', 'laundry-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(26, 3, 'dashboard', 'dashboard-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(27, 3, 'dashboard', 'dashboard-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(28, 3, 'dashboard', 'dashboard-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(29, 3, 'dashboard', 'dashboard-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(30, 3, 'dashboard', 'dashboard-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(31, 3, 'guests', 'guests-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(32, 3, 'guests', 'guests-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(33, 3, 'guests', 'guests-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(34, 3, 'guests', 'guests-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(35, 3, 'guests', 'guests-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(36, 3, 'checkins', 'checkins-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(37, 3, 'checkins', 'checkins-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(38, 3, 'checkins', 'checkins-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(39, 3, 'checkins', 'checkins-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(40, 3, 'checkins', 'checkins-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(41, 3, 'reservation', 'reservation-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(42, 3, 'reservation', 'reservation-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(43, 3, 'reservation', 'reservation-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(44, 3, 'reservation', 'reservation-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(45, 3, 'reservation', 'reservation-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(46, 3, 'room services', 'roomservices-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(47, 3, 'room services', 'roomservices-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(48, 3, 'room services', 'roomservices-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(49, 3, 'room services', 'roomservices-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(50, 3, 'room services', 'roomservices-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(51, 3, 'room types', 'roomtypes-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(52, 3, 'room types', 'roomtypes-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(53, 3, 'room types', 'roomtypes-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(54, 3, 'room types', 'roomtypes-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(55, 3, 'room types', 'roomtypes-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(56, 3, 'rooms', 'rooms-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(57, 3, 'rooms', 'rooms-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(58, 3, 'rooms', 'rooms-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(59, 3, 'rooms', 'rooms-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(60, 3, 'rooms', 'rooms-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(61, 3, 'damages', 'damages-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(62, 3, 'damages', 'damages-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(63, 3, 'damages', 'damages-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(64, 3, 'damages', 'damages-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(65, 3, 'damages', 'damages-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(66, 3, 'discounts', 'discounts-view', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(67, 3, 'discounts', 'discounts-create', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(68, 3, 'discounts', 'discounts-edit', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(69, 3, 'discounts', 'discounts-delete', '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(70, 3, 'discounts', 'discounts-show', '2020-07-30 13:07:45', '2020-07-30 13:07:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_string` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `group_lvl` int(11) NOT NULL DEFAULT '3',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_string`, `name`, `username`, `password`, `group_lvl`, `status`, `created`, `updated`) VALUES
(1, '8XT6W03tZZKkb0BmuPq4kML7sanmOcvaqUwWY1gPQHNfl98TLEnygdIJvtRCFXHR', 'System', 'system', '$2y$10$/lYptDZON2bPtuyVviI5j.RqrJsB//M0U8VEZy0A08aVY6Y4tWnlW', 1, 1, '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(2, 'mu5UqIeLBN0tmJzMW21sgOSaey6TS5ddvEj9n16JLMwQNbiKfrUYrgCxpC4v8wB0', 'Admin', 'admin', '$2y$10$DPDk.RPm9DecEQ68oZOkZevoC0eyWO1QX/6R8wui/3vZPaPeeG3NS', 2, 1, '2020-07-30 13:07:45', '2020-07-30 13:07:45'),
(3, 'HIhcNIvEnXoyfBUbzgsAwW0xQKNT2siQVLg3E4mdJzaCKtGToMGeS2l7FZ8RJcBv', 'Thet Paing Htut', 'hmsadmin', '$2y$10$1PZpD5JIYVl6tOw01e6thOE62jzjMWmFUSZT9T6K8c6i5Sh5NeZz2', 3, 1, '2020-07-31 01:50:54', '2020-10-25 01:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_permissions`
--

INSERT INTO `users_permissions` (`user_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 53),
(2, 54),
(2, 55),
(2, 56),
(2, 57),
(2, 58),
(2, 59),
(2, 60),
(2, 61),
(2, 62),
(2, 63),
(2, 64),
(2, 65),
(2, 66),
(2, 67),
(2, 68),
(2, 69),
(2, 70),
(3, 26),
(3, 31),
(3, 36),
(3, 41),
(3, 42),
(3, 43),
(3, 44),
(3, 45),
(3, 46),
(3, 47),
(3, 48),
(3, 49),
(3, 50),
(3, 51),
(3, 52),
(3, 53),
(3, 54),
(3, 55),
(3, 56),
(3, 57),
(3, 58),
(3, 59),
(3, 60),
(3, 61),
(3, 62),
(3, 63),
(3, 64),
(3, 65),
(3, 66),
(3, 67),
(3, 68),
(3, 69),
(3, 70);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
