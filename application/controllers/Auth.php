<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends My_Controller {
	function __construct() {
		parent::__construct();
        $this->load->model('main_model', 'mmodel');
	}
	public function index() {
		// redirect home page if already logged on
		$this->logged_in();
		$this->load->view('layouts/login_template');
	}
	public function login_action() {
		$this->just_click();
		// redirect home page if already logged on
		$this->logged_in();
    	$username = $this->input->post('username', TRUE);
    	$password = $this->input->post('password', TRUE);
    	$login_state = $this->chk_login($username, $password);
		$this->session->set_flashdata('msg', $login_state['msg']);
		redirect(base_url());
	}
	public function logout_action() {
		$this->logout();
	}

	public function accounts() {
		$this->not_logged_in();
		$this->just_click();
		// check permission
		if (!$this->session->userdata('usermanagement-view')) {
		// if (!$this->session->userdata('usermanagement-view')) {
			redirect('homePage');
		}

		if ($this->session->userdata('group_lvl') == 1) {
			$data = $this->db->get_where('users', ['status' => 1])->result();
		} else {
			$data = $this->db->get_where('users', ['group_lvl >' => 1, 'status' => 1])->result();
		}
		$default = [
			'title' => 'User Management',
			'view' => 'pages/accounts',
			'data' => $data
		];
		$this->load->view('layouts/index', $default);
	}
	public function accounts_create() {
		$this->not_logged_in();
		$this->just_click();
		$id_string = $this->uri->segment(2);
		$title = 'Create';
		$data = null;
		if ($id_string) {
			// check permission
			if (!$this->session->userdata('usermanagement-edit')) {
				redirect('homePage');
			}
			$title = 'Edit';
			$data = $this->db->get_where('users', ['id_string' => $id_string, 'status' => 1])->row();
		} else {
			// check permission
			if (!$this->session->userdata('usermanagement-create')) {
				redirect('homePage');
			}
		}

		$this->db->where('id >', 5);
		$this->db->group_by('suff_name');
		$permissions = $this->db->get('permissions')->result();

		$default = [
			'title' => $title.' User Management',
			'view' => 'pages/accounts_create',
			'data' => $data,
			'permissions' => $permissions
		];
		$this->load->view('layouts/index', $default);
	}
	public function accounts_action() {
		$this->not_logged_in();
		$this->just_click();
		$id_string = $this->input->post('id_string', TRUE);
		$name = $this->input->post('name', TRUE);
		$group = $this->input->post('group', TRUE);
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);

		if ($id_string) {
			// check permission
			if (!$this->session->userdata('usermanagement-edit')) {
				redirect('homePage');
			}
			// edit
			$updated_data = [
				'name' => $name,
				'username' => $username,
				'group_lvl' => $group,
				'updated' => date('Y-m-d H:i:s')
			];
			if ($password) {
				$updated_data['password'] = password_hash($password, PASSWORD_DEFAULT, [24]);
			}
			$updated_state = $this->db->update('users', $updated_data, ['id_string' => $id_string]);
			if ($updated_state) {
				$this->session->set_flashdata('msg', 'Successfully Updated!');
			}
		} else {
			// check permission
			if (!$this->session->userdata('usermanagement-create')) {
				redirect('homePage');
			}
			// create
			$id_string = random_string('alnum', 64);
			$inserted_data = [
				'id_string' => $id_string,
				'name' => $name,
				'username' => $username,
				'password' => password_hash($password, PASSWORD_DEFAULT, [24]),
				'group_lvl' => $group,
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s')
			];
			$inserted_state = $this->db->insert('users', $inserted_data);
			if ($inserted_state) {
				$this->session->set_flashdata('msg', 'Successfully Inserted!');
			}
		}
		redirect('editUser/'.$id_string);
	}
	public function accounts_remove() {
		$this->not_logged_in();
		$this->just_click();
		// check permission
		if (!$this->session->userdata('usermanagement-delete')) {
			redirect('homePage');
		}
		$id_string = $this->uri->segment(2);
		$deleted_state = $this->db->update('users', ['status' => 0], ['id_string' => $id_string]);
		if ($deleted_state) {
			$this->session->set_flashdata('msg', 'Successfully Deleted!');
		}
		redirect('userManagement');
	}
	public function account_permission() {
		$this->not_logged_in();
		$this->just_click();
		// check permission
		if (!$this->session->userdata('usermanagement-edit')) {
			redirect('homePage');
		}

		$permissions = $this->input->post('permission', TRUE);
		$user_id = $this->input->post('user_id', TRUE);
		$id_string = $this->input->post('id_string', TRUE);
		if (isset($permissions)) {
			$this->db->delete('users_permissions', ['user_id' => $user_id]);
			foreach ($permissions as $perm_id) {
				$this->db->insert('users_permissions', ['user_id' => $user_id, 'permission_id' => $perm_id]);
				print_r($perm_id);
			}
		} else {
			$this->db->delete('users_permissions', ['user_id' => $user_id]);
		}
		redirect('editUser/'.$id_string);
	}
}
