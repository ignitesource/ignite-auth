<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Auth extends REST_Controller {
	function __construct() {
		parent::__construct();
        $this->load->database();
        $this->load->model('main_model', 'mmodel');
	}

	public function access_post() {
		$data = $this->input->post();
    	$chk_user = $this->db->get_where('users', ['username' => $data['username']])->row();
        if ($chk_user) {
        	if ($chk_user->status) {
	            if (password_verify($data['password'], $chk_user->password)) {
	                $sess_data = [
	                    'id' => $chk_user->id,
	                    'name' => $chk_user->name,
	                    'username' => $chk_user->username,
	                    'group_lvl_id' => $chk_user->group_lvl,
	                    'group_lvl' => group_type($chk_user->group_lvl)
	                ];

	                // permissions
	                $this->db->select('permissions.*');
	                $this->db->from('permissions');
	                $this->db->join('users_permissions', 'users_permissions.permission_id = permissions.id');
	                $this->db->where('users_permissions.user_id', $chk_user->id);
	                $get_permissions_per_user = $this->db->get()->result();
	                foreach ($get_permissions_per_user as $perm_per_user) {
	                    $sess_data[$perm_per_user->name] = TRUE;
	                }
	                // ok state
	                $res['data'] = $sess_data;
	                $res['status_id'] = 1;
	                $res['status'] = TRUE;
			        http_response_code(200);
	            } else {
	                // wrong password
	                $res['data'] = 'invalid password';
	                $res['status_id'] = 4;
	                $res['status'] = FALSE;
			        http_response_code(401);
	            }
	        } else {
	        	$res['data'] = 'no active';
                $res['status_id'] = 3;
	            $res['status'] = FALSE;
		        http_response_code(401);
	        }
        } else {
            // wrong username or no account
            $res['data'] = 'no username or no account';
            $res['status_id'] = 2;
            $res['status'] = FALSE;
	        http_response_code(401);
        }

		echo json_encode($res);
	}

	public function has_permission_get() {
		$perm_name = $this->uri->segment(2);
		$user_id = $this->uri->segment(3);
		$chk_permission = $this->mmodel->has_permission($perm_name, $user_id);

		if ($chk_permission) {
	        http_response_code(200);
	        echo json_encode(['status' => TRUE]);
		} else {
			http_response_code(403);
	        echo json_encode(['status' => FALSE]);
		}
	}
}