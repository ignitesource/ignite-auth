<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends My_Controller {
	function __construct() {
		parent::__construct();
        $this->load->model('main_model', 'mmodel');
		// redirect to login page if no sess data
		$this->not_logged_in();
	}
	public function index() {
		$default = [
			'title' => 'Home Page',
			'view' => null,
		];
		$this->load->view('layouts/index', $default);
	}
}
