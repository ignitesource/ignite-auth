<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['homePage'] = 'welcome/index';

$route['login'] = 'auth/login_action';
$route['logout'] = 'auth/logout_action';

$route['userManagement'] = 'auth/accounts';
$route['createUser'] = 'auth/accounts_create';
$route['addUser'] = 'auth/accounts_action';
$route['editUser/(:any)'] = 'auth/accounts_create/$1';
$route['deleteUser/(:any)'] = 'auth/accounts_remove/$1';
$route['addPermission'] = 'auth/account_permission';

/*
api
*/

// login state
$route['access']['post'] = 'api/auth/access';

// NOW all permissions move to in session with ** permission name and user id **
$route['has_permission/(:any)/(:num)']['get'] = 'api/auth/has_permission/$permission/$user_id';