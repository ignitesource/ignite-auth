<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();
		// import libraries
		$this->load->library(['session', 'email', 'form_validation', 'pagination', 'calendar', 'zip']);
		// import helpers
        $this->load->helper(['url', 'file', 'form', 'string', 'date', 'inflector']); // singular, plural, camelize, underscore, humanize, is_countable(bool)
        
        date_default_timezone_set('Asia/Rangoon');

        $this->load->database();
        $this->load->model('main_model', 'mmodel');
	}

    protected function chk_login($username, $password) {
        $chk_user = $this->db->get_where('users', ['username' => $username, 'status' => 1])->row();
        if ($chk_user) {
            if (password_verify($password, $chk_user->password)) {
            	$sess_data = [
            		'id' => $chk_user->id,
            		'id_string' => $chk_user->id_string,
            		'name' => $chk_user->name,
            		'username' => $chk_user->username,
            		'group_lvl' => $chk_user->group_lvl,
            		'state' => TRUE,
            	];
                $this->db->select('permissions.*');
                $this->db->from('permissions');
                $this->db->join('users_permissions', 'users_permissions.permission_id = permissions.id');
                $this->db->where('users_permissions.user_id', $chk_user->id);
                $get_permissions_per_user = $this->db->get()->result();
                foreach ($get_permissions_per_user as $perm_per_user) {
                    $sess_data[$perm_per_user->name] = TRUE;
                }
                    
                $this->session->set_userdata($sess_data);
                // ok state
                $data['msg'] = 'Welcome, '.$chk_user->name.'!';
                $data['status'] = TRUE;
            } else {
            	// wrong password
                $data['msg'] = 2;
                $data['status'] = FALSE;
            }
        } else {
        	// wrong username or no account
            $data['msg'] = 1;
            $data['status'] = FALSE;
        }
        return $data;
    }

    protected function logged_in() {
    	if ($this->session->userdata('state')) {
    		redirect('homePage');
    	}
    }

    // redirect login page if no sess data
    protected function not_logged_in() {
    	if (!$this->session->has_userdata('state')) {
    		redirect(base_url());
    	}
    }

    // logout
    protected function logout() {
    	$this->session->sess_destroy();
		redirect(base_url());
    }

	/* ==================================
	check url is redirected or not
	================================== */
	protected function just_click() {
		if (!isset($_SERVER['HTTP_REFERER'])) {
			redirect(base_url());
		}
	}

}