<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table_model extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->dbforge();
	}

	public function create_tbls() {
		/* usres */
		$fields = [
			'id' => ['type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE],
			'id_string' => ['type' => 'VARCHAR', 'constraint' => 191],
			'name' => ['type' => 'VARCHAR', 'constraint' => 191],
			'username' => ['type' => 'VARCHAR', 'constraint' => 191, 'unique' => TRUE],
			'password' => ['type' => 'VARCHAR', 'constraint' => 191],
			'group_lvl' => ['type' => 'INT', 'constraint' => 11, 'default' => 3],
			'status' => ['type' => 'TINYINT', 'constraint' => 1, 'default' => 1],
			'created' => ['type' => 'DATETIME', 'null' => TRUE],
			'updated' => ['type' => 'DATETIME', 'null' => TRUE]
		];
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('users', TRUE);
		$chk_users = $this->db->count_all_results('users');
        if($chk_users < 1) {
            $data = [
            	[
	            	'id_string' => random_string('alnum', 64),
	            	'name' => 'System',
	                'username' => 'system',
	                'password' => password_hash("123123", PASSWORD_DEFAULT, [24]),
	            ],
            	[
	            	'id_string' => random_string('alnum', 64),
	            	'name' => 'Admin',
	                'username' => 'admin',
	                'password' => password_hash("admin123!@#", PASSWORD_DEFAULT, [24]),
	            ]
			];
            $this->db->insert_batch('users', $data);
        }
        
        /* permissions */
        $fields = [
        	'id' => ['type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE],
            'suff_name' => ['type' => 'VARCHAR', 'constraint' => 191],
            'name' => ['type' => 'VARCHAR', 'constraint' => 191],
			'created' => ['type' => 'DATETIME', 'null' => TRUE],
			'updated' => ['type' => 'DATETIME', 'null' => TRUE]
        ];
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('permissions', TRUE);
		$chk_perms = $this->db->count_all_results('permissions');
		if ($chk_perms < 1) {

            $permission_name = ['usermanagement', 'restaurant', 'bar', 'ktv', 'laundry'];
            $allowed_name = ['view', 'create', 'edit', 'delete', 'show'];

            foreach ($permission_name as $per_name) {
                foreach ($allowed_name as $al_name) {
                    $data = [
                        'name' => $per_name.'-'.$al_name,
                    ];
                    $this->db->insert('permissions', $data);
                }
            }
		}

		/* users_permissions */
        $fields = [
        	'user_id' => ['type' => 'INT', 'constraint' => 11],
            'permission_id' => ['type' => 'INT', 'constraint' => 11]
        ];
		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->add_key('permission_id', TRUE);
		$this->dbforge->add_field($fields);
        $this->dbforge->create_table('users_permissions', TRUE);
		$this->dbforge->add_column('users_permissions', ['CONSTRAINT FOREIGN KEY(user_id) REFERENCES users(id)', 'CONSTRAINT FOREIGN KEY(permission_id) REFERENCES permissions(id) ON DELETE CASCADE ON UPDATE CASCADE']);

		$chk_user_perm = $this->db->count_all_results('users_permissions');
		if ($chk_user_perm < 1) {
			$all_perms = $this->db->get('permissions')->result();
			$all_users = $this->db->get('users')->result();
			foreach ($all_users as $user) {
				foreach ($all_perms as $perm) {
					$this->db->insert('users_permissions', ['user_id' => $user->id, 'permission_id' => $perm->id]);
				}
			}
		}
	}
}
