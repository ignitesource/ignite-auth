<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {
	public function __construct() {
		parent::__construct();

		function error_msg($value = null) {
			switch ($value) {
				case '1': return 'username is invalid'; break;
				case '2': return 'password is invalid'; break;
				case '3': return 'account is inactive'; break;
				default: return 'no messsage'; break;
			}
		}
		function group_type($value = null) {
			switch ($value) {
				case '1': return 'System Admin'; break;
				case '2': return 'User Management'; break;
				case '3': return 'Hotel Management'; break;
				case '4': return 'Point of Sale'; break;
				default: return 'NO GROUP'; break;
			}
		}
	}

    // check permission
    public function has_permission($permission, $user_id = null) {
    	if (!$user_id) {
    		$user_id = $this->session->userdata('id');
    	}
		$this->db->select('users_permissions.*');
		$this->db->from('users_permissions');
		$this->db->join('permissions', 'permissions.id = users_permissions.permission_id');
		$this->db->where('permissions.name', $permission);
		$this->db->where('users_permissions.user_id', $user_id);
		$get_permission = $this->db->count_all_results();
		return ($get_permission > 0) ? TRUE : FALSE;
    }
}
