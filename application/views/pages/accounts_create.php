<div class="d-flex">
	<div class="ml-auto">
		<a href="userManagement" class="btn btn-primary rounded-0 mb-3">Back</a>
	</div>
</div>

<div class="kt-portlet">
	<div class="kt-portlet__body">
<?php
	if (isset($data)):
		$id_string = $data->id_string;
		$name = $data->name;
		$username = $data->username;
		$group = $data->group_lvl;
	else:
		$id_string = $name = $username = $group = null;
	endif;
?>
<?= form_open('addUser'); ?>
	<div class="row">
		<div class="col-6">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" name="name" value="<?= $name; ?>" id="name" class="form-control rounded-0" placeholder="Name .." required="">
			</div>
			<div class="form-group">
				<label for="group">Group Type</label>
				<?php if ($this->session->userdata('id_string') == $id_string): ?>
				<input type="text" name="groups" value="<?= group_type($group); ?>" class="form-control rounded-0" readonly>
				<?php else: ?>
				<select name="group" class="form-control rounded-0" required="">
					<option value="2"<?= ($group == 2) ? ' selected' : ''; ?>>User Management</option>
					<option value="3"<?= ($group == 3) ? ' selected' : ''; ?>>Hotel Management</option>
					<option value="4"<?= ($group == 4) ? ' selected' : ''; ?>>Point of Sale</option>
				</select>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-6">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" name="username" value="<?= $username; ?>" id="username" class="form-control rounded-0" placeholder="Username .." required="">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" id="password" class="form-control rounded-0" placeholder="Password .."<?= (isset($data)) ? '' : ' required=""'; ?>>
			</div>
		</div>
	</div>
	<div class="text-right">
		<input type="hidden" name="id_string" value="<?= $id_string; ?>">
		<a href="userManagement" class="btn btn-secondary rounded-0 mr-2">Cancel</a>
		<button type="submit" class=" btn btn-brand rounded-0"><?= (isset($data)) ? 'Update' : 'Create'; ?></button>
	</div>
<?= form_close(); ?>

<?php if (isset($data)): ?>
<?php
$disabled = '';
if ($data->group_lvl <= 2 || $this->session->userdata('id') == $data->id) {
	$disabled = ' disabled';
}
?>
<div class="mt-5">
	<h3>Permissions</h3>
	<div class="table-responsive">
		<?= form_open('addPermission'); ?>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Permission Names</th>
						<th>View</th>
						<th>Create</th>
						<th>Edit</th>
						<th>Delete</th>
						<th>Show</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php if ($this->session->userdata('group_lvl') == 1) { ?>
						<td>User Management</td>
						<td>
							<input type="checkbox" name="permission[]" value="1" id="usermanagement-view" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission('usermanagement-view', $data->id) ? ' checked' : ''; ?>>
							<label for="usermanagement-view" class="sr-only">View</label>
						</td>
						<td>
							<input type="checkbox" name="permission[]" value="2" id="usermanagement-create" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission('usermanagement-create', $data->id) ? ' checked' : ''; ?>>
							<label for="usermanagement-create" class="sr-only">Create</label>
						</td>
						<td>
							<input type="checkbox" name="permission[]" value="3" id="usermanagement-edit" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission('usermanagement-edit', $data->id) ? ' checked' : ''; ?>>
							<label for="usermanagement-edit" class="sr-only">Edit</label>
						</td>
						<td>
							<input type="checkbox" name="permission[]" value="4" id="usermanagement-delete" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission('usermanagement-delete', $data->id) ? ' checked' : ''; ?>>
							<label for="usermanagement-delete" class="sr-only">Delete</label>
						</td>
						<td>
							<input type="checkbox" name="permission[]" value="5" id="usermanagement-show" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission('usermanagement-show', $data->id) ? ' checked' : ''; ?>>
							<label for="usermanagement-show" class="sr-only">Show</label>
						</td>
					</tr>
					<?php } ?>
					<?php if (isset($permissions) && is_array($permissions)) { ?>
						<?php foreach ($permissions as $perm) { ?>
					<tr>
						<td class="text-uppercase"><?= $perm->suff_name; ?></td>
						<!-- FOR ALL PERMISSIONS -->
						<!-- <?php $single_perm = $this->db->get_where('permissions', ['suff_name' => $perm->suff_name])->result(); ?>
						<?php foreach ($single_perm as $single) { ?>
						<td>
							<input type="checkbox" name="permission[]" value="<?= $single->id; ?>" id="<?= $single->name; ?>" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission($single->name, $data->id) ? ' checked' : ''; ?>>
							<label for="<?= $single->name; ?>" class="sr-only">View</label>
						</td>
						<?php } ?> -->
						<td>
							<input type="checkbox" name="permission[]" value="<?= $perm->id; ?>" id="<?= $perm->name; ?>" class="check" <?= $disabled; ?> data-checkbox="icheckbox_square-blue"<?= $this->mmodel->has_permission($perm->name, $data->id) ? ' checked' : ''; ?>>
							<label for="<?= $perm->name; ?>" class="sr-only">View</label>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
    		<div class="row justify-content-center m-0">
	    		<input type="hidden" name="user_id" value="<?= $data->id; ?>">
	    		<input type="hidden" name="id_string" value="<?= $id_string; ?>">
    			<button type="submit" class="col-3 btn btn-success rounded-0"<?= $disabled; ?>>Assign</button>
    		</div>
		<?= form_close(); ?>
	</div>
</div>
<?php endif; ?>
	</div>
</div>

