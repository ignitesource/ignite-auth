<div class="row">
	<div class="col-12">
		
<?php if($this->session->flashdata('msg') != ''): ?>
<div class="alert alert-success">
	<p class="mb-0"><?= $this->session->flashdata('msg'); ?></p>
</div>
<?php endif; ?>
	</div>
</div>

<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon-users-1"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Users
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-actions">
				<?php if ($this->session->userdata('usermanagement-create')): ?>
				<a href="createUser" class="btn btn-label-primary btn-bold btn-icon-h">Create User</a>
				<?php endif; ?>
			</div>
		</div>
	</div>

<div class="table-responsive">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th width="5%" class="text-center">#</th>
				<th>Name</th>
				<th>Username</th>
				<th>Group Type</th>
				<?php if ($this->session->userdata('usermanagement-edit') || $this->session->userdata('usermanagement-delete')): ?>
				<th width="20%" class="text-center">Actions</th>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
			<?php if (isset($data)): ?>
				<?php $n = 0; foreach ($data as $user): ?>
			<tr>
				<td class="text-center"><b><?= ++$n; ?></b></td>
				<td><?= $user->name; ?></td>
				<td><?= $user->username; ?></td>
				<td><?= group_type($user->group_lvl); ?></td>
				<?php if ($this->session->userdata('usermanagement-edit') || $this->session->userdata('usermanagement-delete')): ?>
				<td class="text-center">
					<?php if ($this->session->userdata('usermanagement-edit')): ?>
					<a href="editUser/<?= $user->id_string; ?>" class="text-center btn btn-label-warning btn-bold btn-sm btn-icon-h  <?= ($this->session->userdata('id') > $user->id) ? ' disabled' : ''; ?>"><i class="fa fa-pen"></i></a>
					<?php endif; ?>
					<?php if ($this->session->userdata('usermanagement-delete')): ?>
					<a href="deleteUser/<?= $user->id_string; ?>" onclick="return confirm('Are You Sure to Delete this User?');" class="text-center btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10<?= ($this->session->userdata('id') > $user->id) ? ' disabled' : ''; ?>"><i class="fa fa-trash"></i></a>
					<?php endif; ?>
				</td>
				<?php endif; ?>
			</tr>
				<?php endforeach; ?>
			<?php else: ?>
			<tr>
				<?php if ($this->session->userdata('group_lvl') == 1): ?>
				<td colspan="5" class="text-center">No Data!</td>
				<?php else: ?>
				<td colspan="6" class="text-center">No Data!</td>
				<?php endif; ?>
			</tr>
			<?php endif; ?>
		</tbody>
	</table>
</div>

</div>

