# Usage

It can use with session or API for logging into account and also for
permissions.
There is all permissions in session and if you used via API,
you can type `domain-name/access` and two attributes: `username` and `password`.

Test in postman via `https://ignitesource.net/ignite-auth/access` and set `username`
and `password` in body.
It will give back `user information` and `accesses of permissions` of that username.

And if you need to check permissions of user, use `https://ignitesource.net/ignite-auth/{permissoins}/{user-id}`.

---
